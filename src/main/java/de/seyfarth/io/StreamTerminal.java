package de.seyfarth.io;

import java.io.*;

public class StreamTerminal implements Terminal {

    private final BufferedReader reader;
    private final PrintWriter writer;

    public StreamTerminal(Reader reader, Writer writer) {
        if (reader instanceof BufferedReader) {
            this.reader = (BufferedReader) reader;
        } else {
            this.reader = new BufferedReader(reader);
        }
        if (writer instanceof BufferedWriter) {
            this.writer = (PrintWriter) writer;
        } else {
            this.writer = new PrintWriter(writer);
        }
    }

    @Override
    public void print(Object object) {
        writer.print(object);
        writer.flush();
    }

    @Override
    public void print(char[] charArray) {
        writer.print(charArray);
        writer.flush();
    }

    @Override
    public void printLine(Object object) {
        writer.println(object);
        writer.flush();
    }

    @Override
    public void printLine(char[] charArray) {
        writer.println(charArray);
        writer.flush();
    }

    @Override
    public void printError(String message) {
        printLine("Error, " + message);
    }

    @Override
    public String readLine() {
        print("> ");
        try {
            return reader.readLine();
        } catch (final IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
