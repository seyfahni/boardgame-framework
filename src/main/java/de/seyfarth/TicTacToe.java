package de.seyfarth;

import de.seyfarth.game.moves.AllCoordinatesEmptyMovePolicy;
import de.seyfarth.game.moves.AllTokenCurrentPlayerMovePolicy;
import de.seyfarth.game.moves.AndMovePolicy;
import de.seyfarth.game.moves.ChangeCountMovePolicy;
import de.seyfarth.game.conditions.CountExactSameTokenWinningCondition;
import de.seyfarth.game.Dimension;
import de.seyfarth.game.conditions.DrawAfterMovesWinningCondition;
import de.seyfarth.game.Game;
import de.seyfarth.game.MovePolicy;
import de.seyfarth.game.Player;
import de.seyfarth.game.conditions.PriorityWinningCondition;
import de.seyfarth.game.boards.SquareBoard;
import de.seyfarth.game.WinningCondition;
import de.seyfarth.util.RoundRobbinSequence;
import de.seyfarth.util.Sequence;
import java.util.Arrays;

public class TicTacToe extends Game {

	private static final MovePolicy POLICY = new AndMovePolicy(
			new AllTokenCurrentPlayerMovePolicy(),
			new ChangeCountMovePolicy(count -> count == 1),
			new AllCoordinatesEmptyMovePolicy());
	private static final WinningCondition CONDITION = new PriorityWinningCondition(
			new CountExactSameTokenWinningCondition(3, (center, distance) -> center.move(0, distance)),
			new CountExactSameTokenWinningCondition(3, (center, distance) -> center.move(distance, 0)),
			new CountExactSameTokenWinningCondition(3, (center, distance) -> center.move(distance, distance)),
			new CountExactSameTokenWinningCondition(3, (center, distance) -> center.move(distance, -distance)),
			new DrawAfterMovesWinningCondition(3 * 3 - 1));

	private static Sequence<Player> defaultPlayers() {
		return new RoundRobbinSequence<>(Arrays.asList(new Player("X"), new Player("O")));
	}

	public TicTacToe() {
		this(defaultPlayers());
	}
	
	private TicTacToe(Sequence<Player> players) {
		super(new SquareBoard(new Dimension(3, 3)), POLICY, players, CONDITION);
	}
}
