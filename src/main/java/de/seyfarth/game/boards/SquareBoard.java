package de.seyfarth.game.boards;

import de.seyfarth.game.*;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 *
 */
public class SquareBoard implements Board {

	private final Dimension dimension;
	private final Token[][] content;
	
	private volatile long modifications;

	public SquareBoard(Dimension dimension) {
		this.dimension = Objects.requireNonNull(dimension);
		this.content = new Token[dimension.getHeight()][dimension.getWidth()];
	}

	public Dimension getDimension() {
		return dimension;
	}

	@Override
	public Token getToken(Coordinate coordinate) {
		checkCoordinate(coordinate);
		return content[coordinate.getX()][coordinate.getY()];
	}
	
	@Override
	public void setToken(Coordinate coordinate, Token token) {
		checkCoordinate(coordinate);
		content[coordinate.getX()][coordinate.getY()] = token;
		modifications++;
	}
	
	private void checkCoordinate(Coordinate coordinate) {
		if (!isValidCoordinate(coordinate)) {
			throw new IllegalArgumentException(coordinate + " does not fit inside this board of size " + dimension + ".");
		}
	}

	@Override
	public boolean isValidCoordinate(Coordinate coordinate) {
		Objects.requireNonNull(coordinate);
		return dimension.isInside(coordinate);
	}

	@Override
	public Iterator<FieldState> iterator() {
		return new SquareBoardIterator(modifications);
	}
	
	private class SquareBoardIterator implements Iterator<FieldState> {
		
		private final long version;
		private Coordinate position = new Coordinate(0, 0);

		SquareBoardIterator(long version) {
			this.version = version;
		}
		
		@Override
		public boolean hasNext() {
			checkModification();
			return isValidCoordinate(position);
		}

		@Override
		public FieldState next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}
			FieldState current = new FieldState(position, getToken(position));
			position = position.move(0, 1);
			if (!isValidCoordinate(position)) {
				position = new Coordinate(position.getX() + 1, 0);
			}
			return current;
		}

		private void checkModification() {
			if (version != modifications) {
				throw new ConcurrentModificationException();
			}
		}
	}
}
