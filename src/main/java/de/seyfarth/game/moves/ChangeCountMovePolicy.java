package de.seyfarth.game.moves;

import de.seyfarth.game.Coordinate;
import de.seyfarth.game.GameState;
import de.seyfarth.game.MovePolicy;
import de.seyfarth.game.Token;
import java.util.Map;
import java.util.function.IntPredicate;


public class ChangeCountMovePolicy implements MovePolicy {

	private final IntPredicate changeCountPredicate;

	public ChangeCountMovePolicy(IntPredicate changeCountPredicate) {
		this.changeCountPredicate = changeCountPredicate;
	}

	@Override
	public boolean isValid(Map<Coordinate, Token> changes, GameState state) {
		return changeCountPredicate.test(changes.size());
	}

}
