package de.seyfarth.game;

public interface Token {

	Player getAssociatedPlayer();
	String getLabel();
}
