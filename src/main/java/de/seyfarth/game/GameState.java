package de.seyfarth.game;

import java.util.Iterator;
import java.util.Objects;

public class GameState {

	private final Board board;
	private final Player player;
	private final int move;
	private final GameResult result;
	
	GameState(Board board, Player currentPlayer, int move, GameResult result) {
		this.board = new UnmodifiableBoard(Objects.requireNonNull(board));
		this.player = currentPlayer;
		this.move = move;
		this.result = Objects.requireNonNull(result);
	}

	public Board getBoard() {
		return board;
	}

	public Player getPlayer() {
		return player;
	}

	public int getMove() {
		return move;
	}

	public GameResult getResult() {
		return result;
	}
	
	private class UnmodifiableBoard implements Board {

		private final Board board;
		
		UnmodifiableBoard(Board board) {
			this.board = board;
		}

		@Override
		public Token getToken(Coordinate coordinate) {
			return this.board.getToken(coordinate);
		}

		@Override
		public void setToken(Coordinate coordinate, Token token) {
			throw new UnsupportedOperationException("unmodifiable");
		}

		@Override
		public boolean isValidCoordinate(Coordinate coordinate) {
			return this.board.isValidCoordinate(coordinate);
		}

		@Override
		public Iterator<FieldState> iterator() {
			return this.board.iterator();
		}
	}
}
