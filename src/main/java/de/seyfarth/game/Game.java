package de.seyfarth.game;

import de.seyfarth.util.Sequence;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

public class Game {

	private final Board board;
	private final MovePolicy movePolicy;
	private final Sequence<Player> playerSequence;
	private final WinningCondition winCondition;
	private int move;
	private GameResult result;

	public Game(Board board, MovePolicy movePolicy, Sequence<Player> playerSequence, WinningCondition winCondition) {
		this.board = Objects.requireNonNull(board);
		this.movePolicy = Objects.requireNonNull(movePolicy);
		this.playerSequence = Objects.requireNonNull(playerSequence);
		this.winCondition = Objects.requireNonNull(winCondition);
		this.move = 0;
		this.result = GameResult.undecided();
		checkEnd(Collections.emptyMap());
	}

	public void performMove(Map<Coordinate, Token> changes) {
		if (!isValidMove(changes)) {
			throw new IllegalArgumentException("invalid move.");
		}
		changes.forEach((coordinate, token) -> board.setToken(coordinate, token));
		move++;
		playerSequence.moveNext();
		checkEnd(changes);
	}

	public boolean isValidMove(Map<Coordinate, Token> changes) {
		return changes.keySet().stream().allMatch(coordinate -> board.isValidCoordinate(coordinate))
				&& changes.entrySet().stream()
						.noneMatch(change -> Objects.equals(change.getValue(), board.getToken(change.getKey())))
				&& movePolicy.isValid(changes, getCurrentState());
	}

	private void checkEnd(Map<Coordinate, Token> changes) {
		result = winCondition.getGameResult(changes, getCurrentState());
		if (!isGameOver() && !this.playerSequence.isValid()) {
			this.result = GameResult.draw();
		}
	}

	public Player currentPlayer() {
		if (isGameOver()) {
			throw new IllegalStateException("game over");
		}
		return playerSequence.current();
	}

	public Token getToken(Coordinate coordinate) {
		return board.getToken(coordinate);
	}

	public int getMove() {
		return move;
	}

	public GameResult getResult() {
		return result;
	}

	public GameState getCurrentState() {
		if (isGameOver()) {
			return new GameState(board, null, move, result);
		} else {
			return new GameState(board, playerSequence.current(), move, result);
		}
	}

	public boolean isGameOver() {
		return result.getWinningState() != WinningState.UNDECIDED;
	}
}
