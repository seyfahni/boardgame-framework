package de.seyfarth.game.moves;

import de.seyfarth.game.Coordinate;
import de.seyfarth.game.GameState;
import de.seyfarth.game.MovePolicy;
import de.seyfarth.game.Token;
import java.util.Map;
import java.util.Objects;


public class InvertedMovePolicy implements MovePolicy {

	private final MovePolicy movePolicy;

	public InvertedMovePolicy(MovePolicy movePolicy) {
		this.movePolicy = Objects.requireNonNull(movePolicy);
	}
	
	@Override
	public boolean isValid(Map<Coordinate, Token> changes, GameState state) {
		return !movePolicy.isValid(changes, state);
	}

}
