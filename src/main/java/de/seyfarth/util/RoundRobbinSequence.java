package de.seyfarth.util;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public final class RoundRobbinSequence<E> implements Sequence<E> {

	private final List<E> list;
	private int currentIndex;
	private final boolean valid;

	public RoundRobbinSequence(Collection<? extends E> content) {
		this.list = new ArrayList<>(content);
		this.currentIndex = 0;
		this.valid = !this.list.isEmpty();
	}
	
	@SafeVarargs
	public RoundRobbinSequence(E... content) {
		this.list = Arrays.asList(content);
		this.currentIndex = 0;
		this.valid = !this.list.isEmpty();
	}
	
	@Override
	public void moveNext() {
		currentIndex = (currentIndex + 1) % list.size();
	}

	@Override
	public E current() {
		return list.get(currentIndex);
	}

	@Override
	public boolean isValid() {
		return valid;
	}

}
