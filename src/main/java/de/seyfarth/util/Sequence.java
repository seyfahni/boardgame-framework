package de.seyfarth.util;

/**
 * The sequence in which the elements are ordered. It may be an infinite loop or an ending sequence.<br>
 * <p>
 * Usage:
 * <pre>{@code
 *     while (seq.isValid()) {
 *         E element = seq.current();
 *         ...
 *         seq.moveNext();
 *     }
 * }</pre>
 *
 * @param <E> the type of elements that are ordered
 */
public interface Sequence<E> {

	/**
	 * Move to next element. Should throw an {@link IllegalStateException}, if the sequence already was in an invalid
	 * state.
	 * 
	 * @throws IllegalStateException if the sequence is already invalid
	 * @see #isValid()
	 */
	void moveNext();

	/**
	 * Get the current element. Causes no changes in the sequences order, so repeated calls will always have the same
	 * behaviour.
	 *
	 * @return the current element
	 * @throws IllegalStateException if the sequence is not valid
	 */
	E current();

	/**
	 * Check for validity of the current element. A return value of true indecates that calling any of the other methods
	 * does not result in an exception.
	 *
	 * @return true if there are elements left
	 */
	boolean isValid();
}
