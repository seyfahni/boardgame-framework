package de.seyfarth.game.token;

import de.seyfarth.game.Player;
import de.seyfarth.game.Token;
import java.util.Objects;


public class SimplePlayerToken implements Token {

	private final Player player;
	
	public SimplePlayerToken(Player player) {
		this.player = player;
	}

	@Override
	public Player getAssociatedPlayer() {
		return player;
	}

	@Override
	public String getLabel() {
		return player.getName();
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 71 * hash + Objects.hashCode(this.player);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final SimplePlayerToken other = (SimplePlayerToken) obj;
		if (!Objects.equals(this.player, other.player)) {
			return false;
		}
		return true;
	}

}
