package de.seyfarth;

import de.seyfarth.game.Coordinate;
import de.seyfarth.game.FieldState;
import de.seyfarth.game.Game;
import de.seyfarth.game.token.SimplePlayerToken;
import de.seyfarth.game.Token;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class TicTacToeGUI extends JPanel {

	private static final double SQRT_2 = Math.sqrt(2.0);

	private Game ticTacToe = new TicTacToe();

	private int width = getWidth() - 24;
	private int height = getHeight() - 24;
	private int fieldWidth = width / 3;
	private int fieldHeight = height / 3;
	private int horizontalThickness = Math.max(1, fieldWidth / 10);
	private int horizontalDelta = horizontalThickness * 2 / 3;
	private int verticalThickness = Math.max(1, fieldHeight / 10);
	private int verticalDelta = verticalThickness * 2 / 3;
	private int lineThickness = Math.floorDiv(horizontalThickness + verticalThickness, 2);

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> createAndShowGui());
	}

	public static void createAndShowGui() throws HeadlessException {
		JFrame frame = new JFrame("TicTacToe");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setMinimumSize(new java.awt.Dimension(50, 50));
		frame.setPreferredSize(new java.awt.Dimension(348, 348));
		TicTacToeGUI panel = new TicTacToeGUI();
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				panel.mouseClicked(e);
			}
		});
		panel.setPreferredSize(new java.awt.Dimension(324, 324));
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);
	}

	private void mouseClicked(MouseEvent event) {
		Point clickPos = event.getPoint();
		if (event.getButton() == MouseEvent.BUTTON1
				&& clickPos.x >= 12 && clickPos.x < getWidth() - 12
				&& clickPos.y >= 12 && clickPos.y < getHeight() - 12) {
			if (ticTacToe.isGameOver()) {
				ticTacToe = new TicTacToe();
			} else {
				clickPos.translate(-12, -12);
				Coordinate coord = new Coordinate(clickPos.x / fieldWidth, clickPos.y / fieldHeight);
				Map<Coordinate, Token> change = new HashMap<>();
				change.put(coord, new SimplePlayerToken(ticTacToe.currentPlayer()));
				if (ticTacToe.isValidMove(change)) {
					ticTacToe.performMove(change);
				}
			}
			repaint();
			Toolkit.getDefaultToolkit().sync();
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		width = getWidth() - 24;
		height = getHeight() - 24;
		g.translate(12, 12);
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, width, height);

		fieldWidth = width / 3;
		fieldHeight = height / 3;
		horizontalThickness = Math.max(1, fieldWidth / 10);
		horizontalDelta = horizontalThickness * 2 / 3;
		verticalThickness = Math.max(1, fieldHeight / 10);
		verticalDelta = verticalThickness * 2 / 3;
		lineThickness = Math.floorDiv(7 * (horizontalThickness + verticalThickness), 10);

		g.setColor(Color.BLACK);
		g.fillRect(fieldWidth - horizontalDelta, 0, horizontalThickness, height);
		g.fillRect(width - fieldWidth - horizontalThickness + horizontalDelta, 0, horizontalThickness, height);
		g.fillRect(0, fieldHeight - verticalDelta, width, verticalThickness);
		g.fillRect(0, height - fieldHeight - verticalThickness + verticalDelta, width, verticalThickness);

		for (FieldState field : ticTacToe.getCurrentState().getBoard()) {
			if (field.getToken() != null) {
				switch (field.getToken().getLabel()) {
					case "X":
						paintCross(g, field.getCoordinate());
						break;
					case "O":
						paintCircle(g, field.getCoordinate());
						break;
				}
			}
		}
	}

	private void paintCross(Graphics g, Coordinate coordinate) {
		Rectangle field = calculateField(coordinate);
		resize(field, .65);
		int cornerIndent = (int) Math.ceil(lineThickness / SQRT_2);
		int[] xCoords = new int[] {
			field.x + cornerIndent,
			field.x + field.width,
			field.x + field.width - cornerIndent,
			field.x
		};
		int[] diagonalYCoords = new int[] {
			field.y + field.height,
			field.y + cornerIndent,
			field.y,
			field.y + field.height - cornerIndent
		};
		int[] antiDiagonalYCoords = new int[] {
			field.y,
			field.y + field.height - cornerIndent,
			field.y + field.height,
			field.y + cornerIndent
		};
		g.setColor(Color.BLACK);
		g.fillPolygon(xCoords, diagonalYCoords, 4);
		g.fillPolygon(xCoords, antiDiagonalYCoords, 4);
	}

	private void paintCircle(Graphics g, Coordinate coordinate) {
		Rectangle field = calculateField(coordinate);
		resize(field, .65);
		g.setColor(Color.BLACK);
		g.fillOval(field.x, field.y, field.width, field.height);
		g.setColor(Color.WHITE);
		g.fillOval(field.x + lineThickness, field.y + lineThickness, field.width - 2 * lineThickness, field.height - 2 * lineThickness);
	}

	private Rectangle calculateField(Coordinate coordinate) {
		Rectangle field = new Rectangle();
		switch (coordinate.getX()) {
			case 0:
				field.x = 0;
				field.width = fieldWidth - horizontalDelta;
				break;
			case 1:
				field.x = fieldWidth - horizontalDelta + horizontalThickness;
				field.width = width - fieldWidth - horizontalThickness + horizontalDelta - field.x;
				break;
			case 2:
				field.x = width - fieldWidth + horizontalDelta;
				field.width = fieldWidth - horizontalDelta;
				break;
			default:
				throw new IllegalArgumentException(coordinate.toString());
		}
		switch (coordinate.getY()) {
			case 0:
				field.y = 0;
				field.height = fieldHeight - verticalDelta;
				break;
			case 1:
				field.y = fieldHeight - verticalDelta + verticalThickness;
				field.height = height - fieldHeight - verticalThickness + verticalDelta - field.y;
				break;
			case 2:
				field.y = height - fieldHeight + verticalDelta;
				field.height = fieldHeight - verticalDelta;
				break;
			default:
				throw new IllegalArgumentException(coordinate.toString());
		}
		return field;
	}

	private void resize(Rectangle field, double factor) {
		int w = field.width;
		int h = field.height;
		field.width *= factor;
		field.x += (w - field.width) / 2;
		field.height *= factor;
		field.y += (h - field.height) / 2;
	}
}
