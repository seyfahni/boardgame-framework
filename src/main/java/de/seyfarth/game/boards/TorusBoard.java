package de.seyfarth.game.boards;

import de.seyfarth.game.*;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 *
 */
public class TorusBoard implements Board {

	private final Dimension dimension;
	private final Token[][] content;
	
	private volatile long modifications;

	public TorusBoard(Dimension dimension) {
		this.dimension = Objects.requireNonNull(dimension);
		this.content = new Token[dimension.getHeight()][dimension.getWidth()];
	}

	public Dimension getDimension() {
		return dimension;
	}

	@Override
	public Token getToken(Coordinate coordinate) {
		Coordinate coord = adjustCoordinate(coordinate);
		return content[coord.getX()][coord.getY()];
	}
	
	@Override
	public void setToken(Coordinate coordinate, Token token) {
		Coordinate coord = adjustCoordinate(coordinate);
		content[coord.getX()][coord.getY()] = token;
		modifications++;
	}
	
	private Coordinate adjustCoordinate(Coordinate coordinate) {
		int x = Math.floorMod(coordinate.getX(), dimension.getHeight());
		int y = Math.floorMod(coordinate.getY(), dimension.getWidth());
		return new Coordinate(x, y);
	}

	@Override
	public boolean isValidCoordinate(Coordinate coordinate) {
		Objects.requireNonNull(coordinate);
		return true;
	}

	public boolean isInsideDimensions(Coordinate coordinate) {
		Objects.requireNonNull(coordinate);
		return dimension.isInside(coordinate);
	}

	@Override
	public Iterator<FieldState> iterator() {
		return new SquareBoardIterator(modifications);
	}
	
	private class SquareBoardIterator implements Iterator<FieldState> {
		
		private final long version;
		private Coordinate position = new Coordinate(0, 0);

		SquareBoardIterator(long version) {
			this.version = version;
		}
		
		@Override
		public boolean hasNext() {
			checkModification();
			return isInsideDimensions(position);
		}

		@Override
		public FieldState next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}
			FieldState current = new FieldState(position, getToken(position));
			position = position.move(0, 1);
			if (!isInsideDimensions(position)) {
				position = new Coordinate(position.getX() + 1, 0);
			}
			return current;
		}

		private void checkModification() {
			if (version != modifications) {
				throw new ConcurrentModificationException();
			}
		}
	}
}
