package de.seyfarth;

import de.seyfarth.game.moves.AllCoordinatesEmptyMovePolicy;
import de.seyfarth.game.moves.AllTokenCurrentPlayerMovePolicy;
import de.seyfarth.game.moves.AndMovePolicy;
import de.seyfarth.game.Board;
import de.seyfarth.game.moves.ChangeCountMovePolicy;
import de.seyfarth.game.Coordinate;
import de.seyfarth.game.conditions.CountExactSameTokenWinningCondition;
import de.seyfarth.game.Dimension;
import de.seyfarth.game.conditions.DrawAfterMovesWinningCondition;
import de.seyfarth.game.Game;
import de.seyfarth.game.GameResult;
import de.seyfarth.game.MovePolicy;
import de.seyfarth.game.Player;
import de.seyfarth.game.conditions.PriorityWinningCondition;
import de.seyfarth.game.token.SimplePlayerToken;
import de.seyfarth.game.boards.SquareBoard;
import de.seyfarth.game.Token;
import de.seyfarth.game.boards.TorusBoard;
import de.seyfarth.game.WinningCondition;
import de.seyfarth.io.StreamTerminal;
import de.seyfarth.io.Terminal;
import de.seyfarth.util.RoundRobbinSequence;
import de.seyfarth.util.Sequence;

import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Connect6CLI {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
        Terminal terminal = new StreamTerminal(new InputStreamReader(System.in), new OutputStreamWriter(System.out));
		try {
			Game connect6 = createGame(args);
			playGame(connect6, terminal);
		} catch (IllegalArgumentException ex) {
			terminal.printError(ex.getMessage());
		}
	}

	private static Game createGame(String[] args) {
		if (args.length != 3) {
			throw new IllegalArgumentException("wrong number of command-line arguments. Must be exactly 3.");
		}
		int boardSize = getBoardSize(args[1]);
		Board board = createBoard(args[0], boardSize);

		MovePolicy movePolicy = new AndMovePolicy(
				new AllCoordinatesEmptyMovePolicy(),
				new AllTokenCurrentPlayerMovePolicy(),
				new ChangeCountMovePolicy(changes -> changes == 2));

		Sequence<Player> playerSequence = createPlayers(args[2]);

		WinningCondition winCondition = new PriorityWinningCondition(
				new CountExactSameTokenWinningCondition(6, (center, distance) -> center.move(0, distance)),
				new CountExactSameTokenWinningCondition(6, (center, distance) -> center.move(distance, 0)),
				new CountExactSameTokenWinningCondition(6, (center, distance) -> center.move(distance, distance)),
				new CountExactSameTokenWinningCondition(6, (center, distance) -> center.move(distance, -distance)),
				new DrawAfterMovesWinningCondition(boardSize * boardSize / 2 - 1));

		return new Game(board, movePolicy, playerSequence, winCondition);
	}

	private static int getBoardSize(String arg) {
		int size = parseIntegerArgument(arg);
		if (size < 18 || size > 20 || size % 2 != 0) {
			throw new IllegalArgumentException(size + "is not an allowed board size.");
		}
		return size;
	}

	private static Board createBoard(String type, int size) {
		Dimension dimension = new Dimension(size, size);
		switch (type) {
			case "standard":
				return new SquareBoard(dimension);
			case "torus":
				return new TorusBoard(dimension);
			default:
				throw new IllegalArgumentException("unrecognized board type.");
		}
	}

	private static Sequence<Player> createPlayers(String arg) {
		int playerCount = parseIntegerArgument(arg);
		if (playerCount < 2 || playerCount > 4) {
			throw new IllegalArgumentException(playerCount + "is not an allowed amount of players.");
		}
		List<Player> players = new ArrayList<>(playerCount);
		for (int playerNumber = 1; playerNumber <= playerCount; playerNumber++) {
			players.add(new Player("P" + playerNumber));
		}
		return new RoundRobbinSequence<>(players);
	}

	private static int parseIntegerArgument(String arg) {
		try {
			return Integer.parseInt(arg);
		} catch (NumberFormatException ex) {
			throw new IllegalArgumentException(arg + "is not a number.");
		}
	}

	private static void playGame(Game connect6, Terminal terminal) {
		boolean running = true;
		while (running) {
			String[] input = terminal.readLine().split(" ", 2);
			String command = input[0];
			String[] arguments;
			if (input.length > 1) { // TODO: invalid spacing
				arguments = input[1].split(";");
			} else {
				arguments = new String[0];
			}

			try {
				switch (command) {
					case "place":
						requireArgumentCount(arguments, 2, command);
						place(connect6, terminal, parseCoordinate(arguments[0], arguments[1]));
						break;
					case "state":
						requireArgumentCount(arguments, 2, command);
						state(connect6, terminal, parseCoordinate(arguments[0], arguments[1]));
						break;
					case "print":
						requireArgumentCount(arguments, 0, command);
						print(connect6, terminal);
						break;
					case "quit":
						requireArgumentCount(arguments, 0, command);
						running = false;
						break;
				}
			} catch (CommandException commandException) {
				terminal.printError(commandException.getMessage());
			}
		}
	}

	public static void place(Game connect6, Terminal terminal, Coordinate coordinate) throws CommandException {
		try {
			checkGameRunning(connect6);
			Map<Coordinate, Token> change = new HashMap<>();
			change.put(coordinate, new SimplePlayerToken(connect6.currentPlayer()));
			connect6.performMove(change);
			GameResult result = connect6.getResult();
			switch (result.getWinningState()) {
				case UNDECIDED:
					terminal.printLine("OK");
					break;
				case DRAW:
					terminal.printLine("draw");
					break;
				case WINNER:
					terminal.printLine("P" + result.getWinner()[0].getName() + " wins");
					break;
			}

		} catch (IllegalArgumentException exception) {
			terminal.printError(exception.getMessage());
		}
	}

	public static void state(Game connect6, Terminal terminal, Coordinate coordinate) throws CommandException {
		terminal.printLine(formatToken(connect6.getToken(coordinate)));
	}

	public static void print(Game connect6, Terminal terminal) throws CommandException {
		for (int row = 0; row < 15; row++) {
			StringBuilder line = new StringBuilder(30);
			for (int column = 0; column < 15; column++) {
				line.append(formatToken(connect6.getToken(new Coordinate(row, column))));
				line.append(' ');
			}
			terminal.printLine(line.substring(0, line.length() - 1));
		}
	}

	public static String formatToken(Token token) {
		if (token == null) {
			return "-";
		} else {
			return token.getLabel();
		}
	}

	public static void requireArgumentCount(String[] arguments, int requiredAmount, String command) throws CommandException {
		if (arguments.length != requiredAmount) {
			throw new CommandException(command + " requires " + requiredAmount + " arguments.");
		}
	}

	public static Coordinate parseCoordinate(String first, String second) throws CommandException {
		try {
			int x = Integer.parseInt(first);
			int y = Integer.parseInt(second);
			return new Coordinate(x, y);
		} catch (NumberFormatException numberFormatException) {
			String oldMessage = numberFormatException.getMessage();
			throw new CommandException(oldMessage.substring(19, oldMessage.length() - 1) + " is not a number.");
		}
	}

	private static void checkGameRunning(Game connect6) throws CommandException {
		if (connect6.isGameOver()) {
			throw new CommandException("the game is over.");
		}
	}
}
