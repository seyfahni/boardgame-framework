package de.seyfarth;

import de.seyfarth.game.moves.AllCoordinatesEmptyMovePolicy;
import de.seyfarth.game.moves.AllTokenCurrentPlayerMovePolicy;
import de.seyfarth.game.moves.AndMovePolicy;
import de.seyfarth.game.Board;
import de.seyfarth.game.moves.ChangeCountMovePolicy;
import de.seyfarth.game.Coordinate;
import de.seyfarth.game.conditions.CountExactSameTokenWinningCondition;
import de.seyfarth.game.Dimension;
import de.seyfarth.game.conditions.DrawAfterMovesWinningCondition;
import de.seyfarth.game.Game;
import de.seyfarth.game.GameResult;
import de.seyfarth.game.MovePolicy;
import de.seyfarth.game.Player;
import de.seyfarth.game.conditions.PriorityWinningCondition;
import de.seyfarth.game.token.SimplePlayerToken;
import de.seyfarth.game.boards.SquareBoard;
import de.seyfarth.game.Token;
import de.seyfarth.game.WinningCondition;
import de.seyfarth.io.StreamTerminal;
import de.seyfarth.io.Terminal;
import de.seyfarth.util.RoundRobbinSequence;
import de.seyfarth.util.Sequence;

import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class FiveInARowCLI {

	public static void main(String[] args) {
		Terminal terminal = new StreamTerminal(new InputStreamReader(System.in), new OutputStreamWriter(System.out));

		MovePolicy movePolicy = new AndMovePolicy(
				new AllTokenCurrentPlayerMovePolicy(),
				new ChangeCountMovePolicy(count -> count == 1),
				new AllCoordinatesEmptyMovePolicy());

		Sequence<Player> playerSequence = new RoundRobbinSequence<>(Arrays.asList(new Player("1"), new Player("2")));

		Board board = new SquareBoard(new Dimension(15, 15));

		WinningCondition condition = new PriorityWinningCondition(
				new CountExactSameTokenWinningCondition(5, (center, distance) -> center.move(0, distance)),
				new CountExactSameTokenWinningCondition(5, (center, distance) -> center.move(distance, 0)),
				new CountExactSameTokenWinningCondition(5, (center, distance) -> center.move(distance, distance)),
				new CountExactSameTokenWinningCondition(5, (center, distance) -> center.move(distance, -distance)),
				new DrawAfterMovesWinningCondition(15 * 15 - 1));

		Game fiveInARow = new Game(board, movePolicy, playerSequence, condition);

		boolean running = true;
		while (running) {
			String[] input = terminal.readLine().split(" ", 2);
			String command = input[0];
			String[] arguments;
			if (input.length > 1) { // TODO: invalid spacing
				arguments = input[1].split(";");
			} else {
				arguments = new String[0];
			}

			try {
				switch (command) {
					case "place":
						requireArgumentCount(arguments, 2, command);
						place(fiveInARow, terminal, parseCoordinate(arguments[0], arguments[1]));
						break;
					case "state":
						requireArgumentCount(arguments, 2, command);
						state(fiveInARow, terminal, parseCoordinate(arguments[0], arguments[1]));
						break;
					case "print":
						requireArgumentCount(arguments, 0, command);
						print(fiveInARow, terminal);
						break;
					case "quit":
						requireArgumentCount(arguments, 0, command);
						running = false;
						break;
				}
			} catch (CommandException commandException) {
				terminal.printError(commandException.getMessage());
			}
		}
	}

	public static void place(Game fiveInARow, Terminal terminal, Coordinate coordinate) throws CommandException {
		try {
			checkGameRunning(fiveInARow);
			Map<Coordinate, Token> change = new HashMap<>();
			change.put(coordinate, new SimplePlayerToken(fiveInARow.currentPlayer()));
			fiveInARow.performMove(change);
			GameResult result = fiveInARow.getResult();
			switch (result.getWinningState()) {
				case UNDECIDED:
					terminal.printLine("OK");
					break;
				case DRAW:
					terminal.printLine("draw");
					break;
				case WINNER:
					terminal.printLine("P" + result.getWinner()[0].getName() + " wins");
					break;
			}

		} catch (IllegalArgumentException exception) {
			terminal.printError(exception.getMessage());
		}
	}

	public static void state(Game fiveInARow, Terminal terminal, Coordinate coordinate) throws CommandException {
		terminal.printLine(formatToken(fiveInARow.getToken(coordinate)));
	}

	public static void print(Game fiveInARow, Terminal terminal) throws CommandException {
		for (int row = 0; row < 15; row++) {
			StringBuilder line = new StringBuilder(30);
			for (int column = 0; column < 15; column++) {
				line.append(formatToken(fiveInARow.getToken(new Coordinate(row, column))));
				line.append(' ');
			}
			terminal.printLine(line.substring(0, line.length() - 1));
		}
	}

	public static String formatToken(Token token) {
		if (token == null) {
			return "-";
		} else {
			return token.getLabel();
		}
	}

	public static void requireArgumentCount(String[] arguments, int requiredAmount, String command) throws CommandException {
		if (arguments.length != requiredAmount) {
			throw new CommandException(command + " requires " + requiredAmount + " arguments.");
		}
	}

	public static Coordinate parseCoordinate(String first, String second) throws CommandException {
		try {
			int x = Integer.parseInt(first);
			int y = Integer.parseInt(second);
			return new Coordinate(x, y);
		} catch (NumberFormatException numberFormatException) {
			String oldMessage = numberFormatException.getMessage();
			throw new CommandException(oldMessage.substring(19, oldMessage.length() - 1) + " is not a number.");
		}
	}

	private static void checkGameRunning(Game fiveInARow) throws CommandException {
		if (fiveInARow.isGameOver()) {
			throw new CommandException("the game is over.");
		}
	}
}
