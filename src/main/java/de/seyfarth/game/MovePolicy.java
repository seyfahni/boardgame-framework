package de.seyfarth.game;

import java.util.Map;

/**
 * The policy what moves are allowed.
 */
public interface MovePolicy {

	/**
	 * Checks if the given changes are a valid move.
	 *
	 * @param changes the changes to perform
	 * @param state the current state of the game
	 * @return true if the given changes are a valid move
	 */
	boolean isValid(Map<Coordinate, Token> changes, GameState state);
}
