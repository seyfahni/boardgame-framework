package de.seyfarth.game.conditions;

import de.seyfarth.game.*;
import java.util.Map;


public class DrawAfterMovesWinningCondition implements WinningCondition {

	private final int maxMoves;

	public DrawAfterMovesWinningCondition(int maxMoves) {
		this.maxMoves = maxMoves;
	}
	
	@Override
	public GameResult getGameResult(Map<Coordinate, Token> changes, GameState state) {
		if (state.getMove() > maxMoves) {
			return GameResult.draw();
		} else {
			return GameResult.undecided();
		}
	}

}
