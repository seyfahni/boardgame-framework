package de.seyfarth.game;

/**
 *
 */
public class FieldState {
	private final Coordinate coordinate;
	private final Token token;

	public FieldState(Coordinate coordinate, Token token) {
		this.coordinate = coordinate;
		this.token = token;
	}

	public Coordinate getCoordinate() {
		return coordinate;
	}

	public Token getToken() {
		return token;
	}
	
}
