package de.seyfarth.util;

import java.util.Iterator;

public final class IteratingSequence<E> implements Sequence<E> {

	private final Iterator<E> iterator;
	private E current;
	private boolean valid;

	public IteratingSequence(Iterable<E> iterable) {
		this(iterable.iterator());
	}

	public IteratingSequence(Iterator<E> iterator) {
		this.iterator = iterator;
		update();
	}
	
	@Override
	public void moveNext() {
		checkValid();
		update();
	}

	@Override
	public E current() {
		checkValid();
		return current;
	}

	@Override
	public boolean isValid() {
		return valid;
	}

	private void update() {
		valid = this.iterator.hasNext();
		if (valid) {
			current = iterator.next();
		}
	}
	
	private void checkValid() {
		if (!valid) {
			throw new IllegalStateException();
		}
	}
}
