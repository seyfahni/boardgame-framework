package de.seyfarth.game;

import java.util.Arrays;
import java.util.Objects;

/**
 * The result of a game. This class is immutable.
 * 
 * @see WinningState
 */
public class GameResult {

	private static GameResult UNDECIDED = new GameResult(WinningState.UNDECIDED);
	private static GameResult DRAW = new GameResult(WinningState.DRAW);
	
	/**
	 * Get a GameResult that is undecided.
	 *
	 * @return undecided GameResult
	 */
	public static GameResult undecided() {
		return UNDECIDED;
	}

	/**
	 * Get a GameResult that is decided to be a draw.
	 *
	 * @return draw GameResult
	 */
	public static GameResult draw() {
		return DRAW;
	}

	/**
	 * Get a GameResult that is decided for one or more players. Having no winning players is invalid and will result in
	 * an InvalidArgumentException.
	 *
	 * @param winner the winner(s) of the game
	 * @return win GameResult
	 */
	public static GameResult win(Player... winner) {
		Objects.requireNonNull(winner);
		if (winner.length < 1) {
			throw new IllegalArgumentException("no winner defined");
		}
		return new GameResult(WinningState.WINNER, winner);
	}
	
	private final WinningState state;
	private final Player[] winner;
	
	private GameResult(WinningState winningState) {
		this.state = winningState;
		this.winner = new Player[0];
	}

	private GameResult(WinningState winningState, Player... winner) {
		this.state = winningState;
		this.winner = winner;
	}

	public WinningState getWinningState() {
		return state;
	}

	public Player[] getWinner() {
		return Arrays.copyOf(winner, winner.length);
	}
}
