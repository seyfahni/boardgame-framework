package de.seyfarth.game;

public interface Board extends Iterable<FieldState> {

	Token getToken(Coordinate coordinate);

	void setToken(Coordinate coordinate, Token token);

	boolean isValidCoordinate(Coordinate coordinate);
}
