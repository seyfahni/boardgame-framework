package de.seyfarth.game.moves;

import de.seyfarth.game.Coordinate;
import de.seyfarth.game.GameState;
import de.seyfarth.game.MovePolicy;
import de.seyfarth.game.Token;
import java.util.Map;


public class AllCoordinatesEmptyMovePolicy implements MovePolicy {

	@Override
	public boolean isValid(Map<Coordinate, Token> changes, GameState state) {
		return changes.keySet().stream()
				.map(coordinate -> state.getBoard().getToken(coordinate))
				.allMatch(token -> token == null);
	}

}
