package de.seyfarth.game;

public class Dimension {

	private final int height;
	private final int width;

	public Dimension(int height, int width) {
		if (height < 0 || width < 0) {
			throw new IllegalArgumentException("negative size: (" + height + "," + width + ")");
		}
		this.height = height;
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public boolean isInside(Coordinate coordinate) {
		return coordinate.getX() >= 0 && coordinate.getX() < getHeight()
			&& coordinate.getY() >= 0 && coordinate.getY() < getWidth();
	}

	@Override
	public String toString() {
		return "(" + height + " × " + width + ")";
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 71 * hash + this.height;
		hash = 71 * hash + this.width;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Dimension other = (Dimension) obj;
		if (this.height != other.height) {
			return false;
		}
		if (this.width != other.width) {
			return false;
		}
		return true;
	}
}
