package de.seyfarth;

import de.seyfarth.game.Coordinate;
import de.seyfarth.game.Game;
import de.seyfarth.game.GameResult;
import de.seyfarth.game.token.SimplePlayerToken;
import de.seyfarth.game.Token;
import de.seyfarth.io.StreamTerminal;
import de.seyfarth.io.Terminal;

import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class TicTacToeCLI {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		Terminal terminal = new StreamTerminal(new InputStreamReader(System.in), new OutputStreamWriter(System.out));
		Game ticTacToe = new TicTacToe();

		boolean running = true;
		while (running) {
			String[] input = terminal.readLine().split(" ", 2);
			String command = input[0];
			String[] arguments;
			if (input.length > 1) { // TODO: invalid spacing
				arguments = input[1].split(";");
			} else {
				arguments = new String[0];
			}

			try {
				switch (command) {
					case "place":
						requireArgumentCount(arguments, 2, command);
						place(ticTacToe, terminal, parseCoordinate(arguments[0], arguments[1]));
						break;
					case "state":
						requireArgumentCount(arguments, 2, command);
						state(ticTacToe, terminal, parseCoordinate(arguments[0], arguments[1]));
						break;
					case "print":
						requireArgumentCount(arguments, 0, command);
						print(ticTacToe, terminal);
						break;
					case "quit":
						requireArgumentCount(arguments, 0, command);
						running = false;
						break;
				}
			} catch (CommandException commandException) {
				terminal.printError(commandException.getMessage());
			}
		}
	}

	public static void place(Game ticTacToe, Terminal terminal, Coordinate coordinate) throws CommandException {
		try {
			checkGameRunning(ticTacToe);
			Map<Coordinate, Token> change = new HashMap<>();
			change.put(coordinate, new SimplePlayerToken(ticTacToe.currentPlayer()));
			ticTacToe.performMove(change);
			GameResult result = ticTacToe.getResult();
			switch (result.getWinningState()) {
				case UNDECIDED:
					terminal.printLine("OK");
					break;
				case DRAW:
					terminal.printLine("draw");
					break;
				case WINNER:
					terminal.printLine(result.getWinner()[0].getName() + " wins");
					break;
			}

		} catch (IllegalArgumentException exception) {
			terminal.printError(exception.getMessage());
		}
	}

	public static void state(Game ticTacToe, Terminal terminal, Coordinate coordinate) throws CommandException {
		terminal.printLine(formatToken(ticTacToe.getToken(coordinate)));
	}

	public static void print(Game ticTacToe, Terminal terminal) throws CommandException {
		for (int row = 0; row < 3; row++) {
			StringBuilder line = new StringBuilder(6);
			for (int column = 0; column < 3; column++) {
				line.append(formatToken(ticTacToe.getToken(new Coordinate(row, column))));
				line.append('|');
			}
			terminal.printLine(line.substring(0, line.length() - 1));
			if (row < 2) {
				terminal.printLine("-+-+-");
			}
		}
	}

	public static String formatToken(Token token) {
		if (token == null) {
			return " ";
		} else {
			return token.getLabel();
		}
	}

	public static void requireArgumentCount(String[] arguments, int requiredAmount, String command) throws CommandException {
		if (arguments.length != requiredAmount) {
			throw new CommandException(command + " requires " + requiredAmount + " arguments.");
		}
	}

	public static Coordinate parseCoordinate(String first, String second) throws CommandException {
		try {
			int x = Integer.parseInt(first);
			int y = Integer.parseInt(second);
			return new Coordinate(x, y);
		} catch (NumberFormatException numberFormatException) {
			String oldMessage = numberFormatException.getMessage();
			throw new CommandException(oldMessage.substring(19, oldMessage.length() - 1) + " is not a number.");
		}
	}

	private static void checkGameRunning(Game ticTacToe) throws CommandException {
		if (ticTacToe.isGameOver()) {
			throw new CommandException("the game is over.");
		}
	}
}
