package de.seyfarth.game;

import java.util.Map;

/**
 * The condition to win a game.
 */
public interface WinningCondition {

	/**
	 * Check if the game has been decided by this move.
	 *
	 * @param changes the changes made during this move
	 * @param state the game state after performing the changes
	 * @return the GameResult this move caused
	 */
	GameResult getGameResult(Map<Coordinate, Token> changes, GameState state);
}
