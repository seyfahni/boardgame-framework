package de.seyfarth.game.moves;

import de.seyfarth.game.Coordinate;
import de.seyfarth.game.GameState;
import de.seyfarth.game.MovePolicy;
import de.seyfarth.game.Token;
import java.util.Map;
import java.util.Objects;


public class OrMovePolicy implements MovePolicy {

	private final MovePolicy[] movePolicies;

	public OrMovePolicy(MovePolicy... movePolicies) {
		Objects.requireNonNull(movePolicies);
		if (movePolicies.length < 2) {
			throw new IllegalArgumentException("too few policies");
		}
		this.movePolicies = movePolicies;
	}
	
	@Override
	public boolean isValid(Map<Coordinate, Token> changes, GameState state) {
		for (MovePolicy movePolicy : movePolicies) {
			if (movePolicy.isValid(changes, state)) {
				return true;
			}
		}
		return false;
	}
}
