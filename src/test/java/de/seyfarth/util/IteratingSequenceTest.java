package de.seyfarth.util;

import java.util.Arrays;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class IteratingSequenceTest {

	public IteratingSequenceTest() {
	}

	@SafeVarargs
	public final <E> Iterator<E> iterator(E... elements) {
		return Arrays.asList(elements).iterator();
	}

	@Test
	public void testEmptyIterator() {
		Sequence<Object> seq = new IteratingSequence<>(iterator());
		assertFalse(seq.isValid());
	}

	@Test
	public void testEmptyIteratorMoveNext() {
		Sequence<Object> seq = new IteratingSequence<>(iterator());
		assertThrows(IllegalStateException.class, () -> seq.moveNext());
	}

	@Test
	public void testEmptyIteratorCurrent() {
		Sequence<Object> seq = new IteratingSequence<>(iterator());
		assertThrows(IllegalStateException.class, () -> seq.current());
	}

	@Test
	public void testOneIterator() {
		Object content = new Object();
		Sequence<Object> seq = new IteratingSequence<>(iterator(content));
		assertTrue(seq.isValid());
		assertSame(content, seq.current());
		seq.moveNext();
		assertFalse(seq.isValid());
	}

	@Test
	public void testOneIteratorMoveNext() {
		Object content = new Object();
		Sequence<Object> seq = new IteratingSequence<>(iterator(content));
		seq.moveNext();
		assertThrows(IllegalStateException.class, () -> seq.moveNext());
	}

	@Test
	public void testOneIteratorCurrent() {
		Object content = new Object();
		Sequence<Object> seq = new IteratingSequence<>(iterator(content));
		seq.moveNext();
		assertThrows(IllegalStateException.class, () -> seq.current());
	}

	@Test
	public void testNullIterator() {
		Sequence<Object> seq = new IteratingSequence<>(iterator((Object) null));
		assertTrue(seq.isValid());
		assertNull(seq.current());
		seq.moveNext();
		assertFalse(seq.isValid());
	}

	@Test
	public void testNullIteratorMoveNext() {
		Sequence<Object> seq = new IteratingSequence<>(iterator((Object) null));
		seq.moveNext();
	}

	@Test
	public void testNullIteratorMoveNextTooMuch() {
		Sequence<Object> seq = new IteratingSequence<>(iterator((Object) null));
		seq.moveNext();
		assertThrows(IllegalStateException.class, () -> seq.moveNext());
	}

	@Test
	public void testNullIteratorCurrentAfertMove() {
		Sequence<Object> seq = new IteratingSequence<>(iterator((Object) null));
		seq.moveNext();
		assertThrows(IllegalStateException.class, () -> seq.current());
	}

	@Test
	public void testNullArgumentIterator() {
	    assertThrows(NullPointerException.class, () -> new IteratingSequence<>((Iterator<Object>) null));
	}

	@Test
	public void testNullArgumentIterable() {
	    assertThrows(NullPointerException.class, () -> new IteratingSequence<>((Iterable<Object>) null));
	}
}
