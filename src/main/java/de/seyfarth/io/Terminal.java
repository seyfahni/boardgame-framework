package de.seyfarth.io;

public interface Terminal {

    void print(Object object);

    void print(char[] charArray);

    void printLine(Object object);

    void printLine(char[] charArray);

    void printError(String message);

    String readLine();
}
