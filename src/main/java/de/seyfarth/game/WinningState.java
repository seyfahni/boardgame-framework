package de.seyfarth.game;

/**
 * The of winning a game.
 */
public enum WinningState {

	/**
	 * The game is undecided. There is no player that has won and the game may be continued.
	 */
	UNDECIDED,

	/**
	 * The game is decided to be a draw. The game is over, thus there is no more the possibility to continue the current
	 * game.
	 */
	DRAW,

	/**
	 * One or more player has/have won the game. The game is over, thus there is no more the possibility to continue the
	 * current game.
	 */
	WINNER,;

}
