package de.seyfarth.game.conditions;

import de.seyfarth.game.*;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.IntUnaryOperator;

public class CountAtLeastSameTokenWinningCondition implements WinningCondition {

	private final int amount;
	private final BiFunction<Coordinate, Integer, Coordinate> transformation;

	public CountAtLeastSameTokenWinningCondition(int requiredAmount, BiFunction<Coordinate, Integer, Coordinate> coordinateTransformation) {
		this.amount = requiredAmount;
		this.transformation = Objects.requireNonNull(coordinateTransformation);
	}
	
	@Override
	public GameResult getGameResult(Map<Coordinate, Token> changes, GameState state) {
		for (Map.Entry<Coordinate, Token> change : changes.entrySet()) {
			Token token = change.getValue();
			if (causesWin(change.getKey(), token, state.getBoard())) {
				return GameResult.win(token.getAssociatedPlayer());
			}
		}
		return GameResult.undecided();
	}

	private boolean causesWin(Coordinate center, Token token, Board board) {
		int count = 1;
		count += countToken(board, center, token, d -> d);
		count += countToken(board, center, token, d -> -d);
		return count >= amount;
	}

	private int countToken(Board board, Coordinate center, Token token, IntUnaryOperator distanceTransformation) {
		int count = 0;
		for (int distance = 1; distance <= amount; distance++) {
			Coordinate coordinate = transformation.apply(center, distanceTransformation.applyAsInt(distance));
			if (!isValidToken(board, coordinate, token)) {
				break;
			}
			count++;
		}
		return count;
	}

	private static boolean isValidToken(Board board, Coordinate coordinate, Token token) {
		return board.isValidCoordinate(coordinate) && Objects.equals(token, board.getToken(coordinate));
	}

}
