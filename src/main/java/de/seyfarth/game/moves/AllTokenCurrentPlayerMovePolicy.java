package de.seyfarth.game.moves;

import de.seyfarth.game.Coordinate;
import de.seyfarth.game.GameState;
import de.seyfarth.game.MovePolicy;
import de.seyfarth.game.Token;
import java.util.Map;
import java.util.Objects;

public class AllTokenCurrentPlayerMovePolicy implements MovePolicy {

	public AllTokenCurrentPlayerMovePolicy() {
	}

	@Override
	public boolean isValid(Map<Coordinate, Token> changes, GameState state) {
		return changes.values().stream()
				.allMatch(token -> Objects.equals(token.getAssociatedPlayer(), state.getPlayer()));
	}
}
