package de.seyfarth.game.conditions;

import de.seyfarth.game.*;
import java.util.Map;

/**
 *
 */
public class PriorityWinningCondition implements WinningCondition {

	private final WinningCondition[] priorityOrder;

	/**
	 * Create a prioritised condition order.
	 *
	 * @param priorityOrder
	 */
	public PriorityWinningCondition(WinningCondition... priorityOrder) {
		this.priorityOrder = priorityOrder;
	}

	/**
	 * Check if the game has been decided by this move. The first result that isn't undecided is returned, if there is
	 * no such result, undecided is returned.
	 * 
	 * @param changes the changes made during this move
	 * @param state the game state after performing the changes
	 * @return the GameResult this move caused
	 */
	@Override
	public GameResult getGameResult(Map<Coordinate, Token> changes, GameState state) {
		for (WinningCondition condition : priorityOrder) {
			GameResult result = condition.getGameResult(changes, state);
			if (result.getWinningState() != WinningState.UNDECIDED) {
				return result;
			}
		}
		return GameResult.undecided();
	}

}
